/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.filelab;

import com.mycompany.labflie.Friend;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.jfr.Frequency;

/**
 *
 * @author Arthi
 */
public class WriteFriendList {

    public static void main(String[] args) {
        ArrayList<Friend> friendList = new ArrayList<>();
        friendList.add(new Friend("Arthit", 20, "0615857458"));
        friendList.add(new Friend("Maewline", 2, "0915874589"));
        friendList.add(new Friend("Extra", 20, "0658754145"));
        
        FileOutputStream fos = null;
        try {

            File file = new File("list_of_friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
